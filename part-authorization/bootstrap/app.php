<?php
session_start();

set_exception_handler([new \App\Exceptions\Handler(), 'handle']);
$GLOBALS['my_request'] = new \App\Classes\Request();
$dotEnv = \Dotenv\Dotenv::createImmutable(BASE_PATH);
$dotEnv->load();
require_once BASE_PATH . DIRECTORY_SEPARATOR . 'routes/routes.php';
