<?php

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

$request = \Laminas\Diactoros\ServerRequestFactory::fromGlobals(
  $_SERVER, $_GET, $_POST, $_COOKIE, $_FILES
);

$router = new \League\Route\Router();

$router->group('api', function (\League\Route\RouteGroup $route) {
    $route->post('/client/create', [\App\Controller\ClientController::class, 'create']);
    $route->post('/access/check', [\App\Controller\AccessController::class, 'checkAccess']);
    $route->post('/access/store', [\App\Controller\AccessController::class, 'storeAccess']);

})->middleware(new \App\Middlewares\ClientMiddleware());

$response = $router->dispatch($request);
(new Laminas\HttpHandlerRunner\Emitter\SapiEmitter)->emit($response);