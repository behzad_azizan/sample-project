<?php

const BASE_PATH = __DIR__ . DIRECTORY_SEPARATOR . '..';
const PUBLIC_PATH = __DIR__ ;


require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../bootstrap/app.php';
