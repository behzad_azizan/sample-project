<?php

namespace App\Exceptions;



class Handler
{
    /**
     * error handling
     * @param \Exception $exception
     */
    public function handle($exception)
    {
        if ($exception instanceof InvalidTokenException)
            return response()->setStatusCode(401)->errorResponse(['error' => 'invalid_jwt_token']);
        if ($exception instanceof InvalidClientException)
            return response()->setStatusCode(401)->errorResponse(['error' => 'invalid_client_secret']);

        return response()->setStatusCode(500)->errorResponse(['error' => $exception->getMessage()]);
    }
}