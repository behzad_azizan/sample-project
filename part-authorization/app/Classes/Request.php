<?php

namespace App\Classes;


class Request
{

    protected $query;
    protected $postData;
    protected $request;
    protected $files;
    protected $method;
    protected $ip;
    protected $cookie;


    /**
     * Request constructor.
     * @param null $method
     * @param array|null $query
     * @param array|null $postData
     * @param array|null $request
     * @param array|null $files
     * @param null $ip
     * @param null $cookie
     * @param null $raw
     */
    public function __construct($method = null, array $query = null, array $postData = null, array $request = null, array $files = null, $ip = null, $cookie = null, $raw = null)
    {
        $this->setQuery($query)
            ->setPostData($postData)
            ->setRaw($raw)
            ->setMethod($method)
            ->setRequest($request)
            ->setFiles($files)
            ->setIp($ip)
            ->setCookie($cookie);

    }

    public function setRaw($raw = null) {

        $data = $raw ?? file_get_contents('php://input');
        $this->raw = json_decode($data, true);
        return $this;
    }

    public function getRaw($parameter = null, $default = null)
    {
        if ($parameter)
            return $this->raw[$parameter] ?? $default;

        return $this->raw;
    }

    /**
     * set get method
     * @param array|null $data
     * @return $this
     */
    public function setQuery(array $data = null)
    {
        $this->query = is_array($data) ? $data : $_GET;
        return $this;
    }

    /**
     * set post method
     * @param array|null $data
     * @return $this
     */
    public function setPostData(array $data = null)
    {
        $this->postData = is_array($data) ? $data : $_POST;
        return $this;
    }

    /**
     * set request data
     * @param array|null $data
     * @return $this
     */
    public function setRequest(array $data = null)
    {
        $this->request = is_array($data) ? $data : $_REQUEST;
        return $this;
    }

    /**
     * set file data
     * @param array|null $data
     * @return $this
     */
    public function setFiles(array $data = null)
    {
        $this->files = is_array($data) ? $data : $_FILES;
        return $this;
    }

    /**
     * set method
     * @param null $method
     * @return $this
     */
    public function setMethod($method = null)
    {
        $this->request = is_string($method) ? $method : $_SERVER['REQUEST_METHOD'];
        return $this;
    }

    /**
     * set ip
     * @param null $ip
     * @return $this
     */
    public function setIp($ip = null)
    {
        $this->ip = $ip ?? $_SERVER['REMOTE_ADDR'];
        return $this;
    }

    /**
     * set cookie
     * @param array|null $cookie
     * @return $this
     */
    public function setCookie(array $cookie = null)
    {
        $this->cookie = is_array($cookie) ? $cookie : $_COOKIE;
        return $this;
    }

    public function getMethod()
    {
        return $this->method;
    }

    /**
     * get GET method
     * @param null $parameter
     * @param null $default
     * @return array|null
     */
    public function getQuery($parameter = null, $default = null)
    {
        if ($parameter)
            return $this->query[$parameter] ?? $default;

        return $this->query;
    }

    /**
     * set POST method
     * @param null $parameter
     * @param null $default
     * @return array|null
     */
    public function getData($parameter = null, $default = null)
    {
        if ($parameter)
            return $this->postData[$parameter] ?? $default;

        return $this->postData;
    }

    /**
     * get request
     * @return array
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * get ip
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * get special request
     * @param $name
     * @param null $default
     * @return |null
     */
    public function get($name, $default = null)
    {
        return $this->request[$name] ?? $default;
    }

    public function getHeader($key)
    {
        return getallheaders()[$key] ?? null;
    }

    /**
     * @return array
     */
    public function getClient()
    {
        return $_SESSION['client'];
    }

}