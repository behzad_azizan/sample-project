<?php


namespace App\Classes;


class Database
{
    /**
     * @var \PDO
     */
    private static $pdo = null;
    protected function __construct()
    {
    }

    /**
     * @return \PDO
     */
    public static function getPdo()
    {
        if (self::$pdo)
            return self::$pdo;

        $user = getenv('MYSQL_USERNAME');
        $passwd = getenv("MYSQL_PASSWORD");
        $host = getenv("MYSQL_HOST");
        $db = getenv("MYSQL_DATABASE");
        $dsn = "mysql:host={$host};dbname={$db}";
        self::$pdo = new \PDO($dsn, $user, $passwd);
        return self::$pdo;
    }
}