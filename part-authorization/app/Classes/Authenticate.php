<?php


namespace App\Classes;


use App\Exceptions\InvalidTokenException;

class Authenticate
{
    protected $apiUrl = 'http://first-part.loc/api/v1/auth/';
    public static function getInstance()
    {
        return new self();
    }

    public function getUser($jwt)
    {
        try {
            $user = $this->exec('profile', [
                'headers' => [
                    'Authorization' => "Bearer {$jwt}"
                ]
            ], 'get');

        }catch (\Exception $e) {
            throw new InvalidTokenException();
        }

        return $user;
    }

    /**
     * @param string $api
     * @param array $params
     * @param string $method
     * @return mixed
     */
    private function exec(string $api, array $params, $method = 'post')
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->request($method, $this->apiUrl . $api, $params);
        $out = json_decode($res->getBody(), true);

        return $out['data'];
    }
}