<?php
function pd()
{
    // if (!defined('DEBUG')) return;
    foreach (func_get_args() as $index => $arg) {
        echo '<pre><h4>Param : ' . $index . '</h4>';
        if (gettype($arg) == 'NULL' || gettype($arg) == 'boolean')
            var_dump($arg);
        else
            print_r($arg);

        echo '</pre>';
    }
    die();
}

function public_path($path = '')
{
    return PUBLIC_PATH . DIRECTORY_SEPARATOR . $path;
}

function response()
{
    $out = new \App\Classes\Response();
    return $out;
}

/**
 * @return \App\Classes\Request
 */
function request()
{
    return $GLOBALS['my_request'];
}


/**
 * @return PDO
 */
function pdo()
{
    return \App\Classes\Database::getPdo();
}