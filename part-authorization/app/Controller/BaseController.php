<?php

namespace App\Controller;

class BaseController
{
    public function getRequest()
    {
        return request();
    }
}