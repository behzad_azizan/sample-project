<?php


namespace App\Controller;


use App\Classes\Authenticate;
use App\Repositories\UserAccessRepository;

class AccessController extends BaseController
{
    /**
     * @var UserAccessRepository
     */
    private $repository;

    public function __construct()
    {
        $this->repository = UserAccessRepository::getInstance();
    }

    public function checkAccess()
    {
        $userId = $_SESSION['user']['id'];
        $permission = $this->getRequest()->getRaw('permission');

        $status = $this->repository->checkAccess($userId, $permission);
        return response()->successResponse([
            'access' => $status
        ]);
    }

    public function storeAccess()
    {
        $userId = $_SESSION['user']['id'];
        $permission = $this->getRequest()->getRaw('permission');
        $this->repository->store($userId, $permission);

        return response()->successResponse([
            'status' => 'true'
        ]);
    }
}