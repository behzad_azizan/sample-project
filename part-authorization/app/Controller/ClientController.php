<?php


namespace App\Controller;


use App\Repositories\ClientRepository;

class ClientController extends BaseController
{
    /**
     * @var ClientRepository
     */
    private $reposotory;

    public function __construct()
    {
        $this->reposotory = ClientRepository::getInstance();
    }

    public function create()
    {
        $data = [
            'name' => $this->getRequest()->get('name')
        ];

        $result = $this->reposotory->createClient($data);
        return response()->successResponse([
            'status' => 'true',
            'client' => $result
        ]);
    }
}