<?php


namespace App\Middlewares;


use App\Classes\Authenticate;
use App\Exceptions\InvalidClientException;
use App\Exceptions\InvalidTokenException;
use App\Repositories\ClientRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ClientMiddleware implements MiddlewareInterface
{

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $except = [

        ];
        $headers = getallheaders();
        if (in_array($request->getRequestTarget(), $except))
            return $handler->handle($request);

        if (! $bearer = $this->getBearer($headers))
            throw new InvalidTokenException();

        $_SESSION['user'] = Authenticate::getInstance()->getUser($bearer);
        return $handler->handle($request);
    }

    protected function getBearer($header)
    {
        if (! isset($header['Authorization']))
            return null;

        return str_replace('Bearer ', '', $header['Authorization']);
    }
}