<?php


namespace App\Repositories;


interface RepositoryInterface
{
    public static function getInstance();
}