<?php
namespace App\Repositories;

use App\Classes\Mongo;
use App\Exceptions\InvalidClientException;
use App\Traits\Validator;
use MongoCollection;
use MongoDB;

class ClientRepository extends BaseRepository
{
    use Validator;

    /**
     * Create new client
     * @param array $data
     * @return array
     * @throws InvalidClientException
     */
    public function createClient(array $data)
    {
        $data = $this->_createClient($data);
        $sql = "INSERT INTO clients (name, client_secret) VALUES (?,?)";
        $stmt= pdo()->prepare($sql);
        $stmt->execute([$data['name'], $data['secret']]);

        return $this->getClientWithSecret($data['secret']);
    }

    /**
     * @param $clientSecret
     * @return mixed
     * @throws InvalidClientException
     */
    public function getClientWithSecret($clientSecret)
    {
        $sql = 'select * FROM clients WHERE client_secret = ?';
        $stmt= pdo()->prepare($sql);
        $stmt->execute([$clientSecret]);
        if (! $stmt->rowCount())
            throw new InvalidClientException();

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    protected function _createClient($data)
    {
        return [
            'name' => $data['name'],
            'secret' => $this->generateSecret()
        ];
    }

    /**
     * Generate client secret token
     * @return string
     * @throws \Exception
     */
    protected function generateSecret()
    {
        return bin2hex(random_bytes(32));
    }

    /**
     * @return ClientRepository
     */
    public static function getInstance()
    {
        return new self();
    }
}