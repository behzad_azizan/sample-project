<?php


namespace App\Repositories;


use App\Classes\Request;

class UserAccessRepository extends BaseRepository
{
    public function store($userId, $rule)
    {
        $client = \request()->getClient();
        $sql = "INSERT INTO users_access (user_id, role) VALUES (?,?)";
        $stmt= pdo()->prepare($sql);
        return $stmt->execute([$userId, $rule]);
    }

    public function checkAccess($userId, $role)
    {
        $sql = "select COUNT(1) as cnt FROM users_access WHERE user_id = ? AND role = ?";
        $stmt= pdo()->prepare($sql);
        $stmt->execute([$userId, $role]);
        return (bool) $stmt->fetch(\PDO::FETCH_ASSOC)['cnt'];
    }

    /**
     * @return UserAccessRepository
     */
    public static function getInstance()
    {
        return new self();
    }
}