<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'name' => 'Behzad Azizan',
            'email' => 'behzad.azizan1991@gmail.com',
            'password' => Hash::make('12345678')
        ]);
    }
}
