<?php


namespace App\Traits;


use App\Exceptions\PermissionException;

trait CheckPermission
{
    protected $apiUrl = 'http://part-authorization.loc/api/';

    /**
     * @param $operation
     * @return bool
     * @throws PermissionException
     */
    public function can($operation)
    {
        $jwt = request()->bearerToken();
        try {
            $status = $this->exec('access/check', [
                'json' => [
                    'permission' => $operation
                ],
                'headers' => [
                    'Authorization' => "Bearer {$jwt}"
                ]
            ]);
            if (! $status['access'])
                throw new \Exception();

            return true;
        }catch (\Exception $exception) {
            throw new PermissionException();
        }
    }

    /**
     * @param string $api
     * @param array $params
     * @param string $method
     * @return mixed
     */
    private function exec(string $api, array $params, $method = 'post')
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->request($method, $this->apiUrl . $api, $params);
        $out = json_decode($res->getBody(), true);
        return $out['data'];
    }
}
