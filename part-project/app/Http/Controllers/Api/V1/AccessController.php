<?php


namespace App\Http\Controllers\Api\V1;


use App\Traits\CheckPermission;

class AccessController extends BaseController
{
    use CheckPermission;

    public function testAccess()
    {
        $this->can('foo.bar');

    }
}
