<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BaseController extends Controller
{
    /**
     * @param string $message
     * @param array|JsonResource $data
     * @return \Illuminate\Http\JsonResponse
     */
    protected function successResponse($data = [], string $message = null)
    {
        if (! $message)
            $message = trans('main.success');

        return response()->json([
            'status' => 1,
            'message' => $message,
            'data' => $data
        ]);
    }


    protected function failedResponse($data = [], string $message = null)
    {
        if (! $message)
            $message = trans('main.failed');

        return response()->json([
            'status' => 0,
            'message' => $message,
            'data' => $data
        ]);
    }
}
