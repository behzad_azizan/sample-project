<?php


namespace App\Repositories;


use App\Exceptions\InvalidAuthException;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserRepository extends BaseRepository
{
    const LOGIN_TYPE_JWT = 'jwt';

    /**
     * Login with username and password
     * @param $email
     * @param $password
     * @param string $type
     * @return bool
     * @throws InvalidAuthException
     */
    public function login($email, $password, $type = self::LOGIN_TYPE_JWT)
    {
        $credentials = [
            'email' => $email,
            'password' => $password
        ];

        switch ($type) {
            case self::LOGIN_TYPE_JWT:
                if (! $result = auth()->attempt($credentials))
                    throw new InvalidAuthException();

                return $result;
        }
    }

    /**
     * Register a new user
     * @param array $data
     * @return User
     */
    public function register(array $data)
    {
        $user = new User($data);
        $user->password = Hash::make($data['password']);
        $user->save();

        return $user;
    }
}
