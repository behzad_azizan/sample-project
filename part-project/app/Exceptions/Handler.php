<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(\Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  Request  $request
     * @param  \Throwable  $e
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, \Throwable $e)
    {
        if ($e instanceof ModelNotFoundException)
            return response()->json([
                'status' => 0,
                'error' => 'not_fount',
                'message' => 'رکورد یافت نشد!',
                'data' => null
            ], 404);
        elseif ($e instanceof NotFoundHttpException )
            return response()->json([
                'status' => 0,
                'error' => 'not_fount_route',
                'message' => 'هیچ مسیری برای پاسخگویی به درخواست شما یافت نشد!',
                'data' => null
            ], 404);
        elseif ($e instanceof ValidationException)
            return response()->json([
                'status' => 0,
                'error' => 'validation_error',
                'message' => 'ورودی های ارسالی نامعتبر است.',
                'data' => $e->errors()
            ], 400);
        elseif($e instanceof AuthenticationException)
            return response()->json([
                'status' => 0,
                'error' => 'Invalid_token',
                'message' => 'کلید اعتبارسنجی ارسالی نامعتبر است.',
                'data' => null,
            ], 401);
        elseif($e instanceof InvalidAuthException)
            return response()->json([
                'status' => 0,
                'error' => 'invalid_auth',
                'message' => 'نام کاربری یا رمز عبور نامعتبر است.',
                'data' => null,
            ], 400);
        elseif($e instanceof PermissionException)
            return response()->json([
                'status' => 0,
                'error' => 'authorization_error',
                'message' => 'شما مجاز به اجرای این عملیات نیستید.',
                'data' => null,
            ], 400);
        elseif ($e instanceof HttpException)
            return response()->json([
                'status' => 0,
                'error' => 'http_error',
                'message' => $e->getMessage() ? $e->getMessage() : 'خطای عدم امکان دسترسی به این قسمت!',
                'data' => null
            ], $e->getStatusCode());


        $errorCode = $e->getCode() && in_array($e->getCode(), array_keys(Response::$statusTexts)) ? $e->getCode() : 500;

        return response()->json([
            'status' => 0,
            'error' => 'internal_error',
            'message' => $e->getMessage(),
            'data' => null
        ], $errorCode);
    }
}
