<?php
return [
    'success' => 'The operation was successful.',
    'failed' => 'An error occurred'
];
