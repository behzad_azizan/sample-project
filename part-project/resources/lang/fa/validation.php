<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => ':attribute باید بعد از :date باشد',
    'after_or_equal'       => 'The :attribute must be a date after or equal to :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => ':attribute باید قبل از :date باشد',
    'before_or_equal'      => 'The :attribute must be a date before or equal to :date.',
    'between'              => [
        'numeric' => ':attribute باید عددی بین :min و :max باشد',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'فیلد :attribute باید مقدار 0 یا 1 داشته باشد',
    'confirmed'            => 'فیلد :attribute یکسان نمی باشد',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'فرمت :attribute باید به صورت :format باشد',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => ':attribute باید یک عدد :digits رقمی باشد',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'لطفا فیلد :attribute را به صورت صحیح پر کنید',
    'exists'               => 'این :attribute وجود ندارد',
    'file'                 => 'The :attribute must be a file.',
    'filled'               => 'The :attribute field must have a value.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => ':attribute باید مقدار عددی صحیح داشته باشد',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'ipv4'                 => 'The :attribute must be a valid IPv4 address.',
    'ipv6'                 => 'The :attribute must be a valid IPv6 address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'فیلد :attribute نمی تواند از :max بزرگتر باشد',
        'file'    => 'مقدار :attribute باید حداکثر :max کیلوبایت باشد',
        'string'  => 'فیلد :attribute نمی تواند از :max کاراکتر بیشتر باشد',
        'array'   => 'فیلد :attribute نمی تواند از :max آیتم بیشتر باشد',
    ],
    'mimes'                => 'فایل :attribute باید یکی از فرمت های :values باشد',
    'mimetypes'            => 'فایل :attribute باید یکی از فرمت های :values باشد',
    'min'                  => [
        'numeric' => 'فیلد :attribute نمی تواند از :min کوچکتر باشد',
        'file'    => 'مقدار :attribute باید حداقل :min کیلوبایت باشد',
        'string'  => 'فیلد :attribute نمی تواند از :min کاراکتر کمتر باشد',
        'array'   => 'فیلد :attribute نمی تواند از :min آیتم کمتر باشد',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'not_regex'            => 'The :attribute format is invalid.',
    'numeric'              => ':attribute باید یک عدد باشد',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => ':attribute الزامی می باشد',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'طول فیلد :attribute باید :size کاراکتر/رقم باشد',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'فیلد :attribute باید به صورت متنی(رشته) باشد',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'این :attribute قبلا در سیستم ثبت شده است',
    'uploaded'             => 'The :attribute failed to upload.',
    'url'                  => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'fname' => 'نام',
        'lname' => 'نام خانوادگی',
        'username' => 'نام کاربری',
        'email' => 'ایمیل',
        'password' => 'کلمه عبور',
        'password_confirmation' => 'تایید کلمه عبور',
        'mobile' => 'شماره موبایل',
        'verification_code' => 'کد تایید',
        'phone' => 'تلفن ثابت',
        'national_code' => 'کد ملی',
        'zip_code' => 'کدپستی',
        'old_password' => 'کلمه عبور فعلی',
        'reset_code' => 'کد بازیابی',
        'avatar' => 'تصویر آواتار',
        'status' => 'وضعیت کاربری',
        'verified' => 'وضعیت تایید کاربر',
        'en_name' => 'نام لاتین',
        'fa_name' => 'نام فارسی',
        'start_date' => 'تاریخ رفت',
        'end_date' => 'تاریخ برگشت',
        'guest' => 'مهمان',

    ],

];
