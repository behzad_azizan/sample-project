<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'کلمه های عبور باید یکسان و حداقل 8 کاراکتر باشد!',
    'reset' => 'کلمه عبور شما با موفقیت تغییر نمود!',
    'sent' => 'ایمیل حاوی کلمه عبور برای شما ارسال شد!',
    'token' => 'لینک بازیابی کلمه عبور منقضی شده یا وجود ندارد!',
    'user' => "کاربری با آدرس ایمیل وارد شده یافت نشد!",

];
