<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'اطلاعات وارد شده برای ورود به سیستم صحیح نمی باشد!',
    'throttle' => 'دسترسی شما بنا به دلایل امنیتی به مدت :seconds ثانیه قطع گردید!',

];
